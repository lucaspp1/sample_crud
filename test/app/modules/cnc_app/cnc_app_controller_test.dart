import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:sample_crud/app/modules/cnc_app/cnc_app_controller.dart';
import 'package:sample_crud/app/modules/cnc_app/cnc_app_module.dart';
import 'package:sample_crud/app/shared/repository/UserRepository.dart';
import 'package:sample_crud/app/shared/repository/interface/IUserRepository.dart';

void main() {
  initModule(CncAppModule());
  CncAppController cncapp;

  setUp(() {
    cncapp = CncAppModule.to.get<CncAppController>();
  });

  group('CncAppController Test', () {
    test("First Test", () {
      expect(cncapp, isInstanceOf<CncAppController>());
    });

    test("dependecy teste", () {
      initModule(CncAppModule(), changeBinds: [
        Bind<IUserRepository>((i) => UserRepository()),
      ]);
      expect(Modular.get<IUserRepository>(), isA<UserRepository>());  
    });

    test("Set Value", () {
      expect(cncapp.value, equals(0));
      cncapp.increment();
      expect(cncapp.value, equals(1));
    });
  });
}
