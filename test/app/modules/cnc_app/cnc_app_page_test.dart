import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:sample_crud/app/modules/cnc_app/cnc_app_page.dart';

main() {
  testWidgets('CncAppPage has title', (WidgetTester tester) async {
    await tester.pumpWidget(buildTestableWidget(CncAppPage(title: 'CncApp')));
    final titleFinder = find.text('CncApp');
    expect(titleFinder, findsOneWidget);
  });
}
