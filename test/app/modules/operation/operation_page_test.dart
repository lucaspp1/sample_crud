import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:sample_crud/app/modules/operation/operation_page.dart';

main() {
  testWidgets('OperationPage has title', (WidgetTester tester) async {
    await tester
        .pumpWidget(buildTestableWidget(OperationPage(title: 'Operation')));
    final titleFinder = find.text('Operation');
    expect(titleFinder, findsOneWidget);
  });
}
