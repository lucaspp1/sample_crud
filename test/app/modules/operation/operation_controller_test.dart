import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:sample_crud/app/modules/operation/operation_controller.dart';
import 'package:sample_crud/app/modules/operation/operation_module.dart';

void main() {
  initModule(OperationModule());
  OperationController operation;

  setUp(() {
    operation = OperationModule.to.get<OperationController>();
  });

  group('OperationController Test', () {
    test("First Test", () {
      expect(operation, isInstanceOf<OperationController>());
    });

    test("Set Value", () {
      expect(operation.value, equals(0));
      operation.increment();
      expect(operation.value, equals(1));
    });
  });
}
