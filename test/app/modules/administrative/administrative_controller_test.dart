import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:sample_crud/app/modules/administrative/administrative_controller.dart';
import 'package:sample_crud/app/modules/administrative/administrative_module.dart';

void main() {
  initModule(AdministrativeModule());
  AdministrativeController administrative;

  setUp(() {
    administrative = AdministrativeModule.to.get<AdministrativeController>();
  });

  group('AdministrativeController Test', () {
    test("First Test", () {
      expect(administrative, isInstanceOf<AdministrativeController>());
    });

    test("Set Value", () {
      expect(administrative.value, equals(0));
      administrative.increment();
      expect(administrative.value, equals(1));
    });
  });
}
