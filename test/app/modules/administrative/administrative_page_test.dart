import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:sample_crud/app/modules/administrative/administrative_page.dart';

main() {
  testWidgets('AdministrativePage has title', (WidgetTester tester) async {
    await tester.pumpWidget(
        buildTestableWidget(AdministrativePage(title: 'Administrative')));
    final titleFinder = find.text('Administrative');
    expect(titleFinder, findsOneWidget);
  });
}
