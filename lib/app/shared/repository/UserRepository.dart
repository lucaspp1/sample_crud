import 'package:sample_crud/app/shared/SharedClasses.dart';
import 'package:sample_crud/app/shared/domain/User.dart';
import 'package:sample_crud/app/shared/repository/interface/IUserRepository.dart';

class UserRepository implements IUserRepository {
  @override@override
  Future<bool> delete(User object) {
    return Future.value(SharedClasses.getListUser().remove(object));
  }

  @override
  Future<bool> insert(User object) {
    SharedClasses.getListUser().add(object);
    return Future.value(true);
  }

  @override
  Future<List<User>> searchAll() {
    return Future.value(SharedClasses.getListUser());
  }

  @override
  Future<User> searchByid(int id) {
    return Future.value(SharedClasses.getListUser().where((x) => x.id == id).first);
  }

  @override
  Future<bool> update(User object) {
    try {
      int index = SharedClasses.getListUser().indexOf(
          SharedClasses.getListUser().where((x) => x.id == object.id).first);
      SharedClasses.getListUser()[index] = object;
      return Future.value(true);
    } on Exception catch (e) {
      return Future.value(false);
    }
  }
}
