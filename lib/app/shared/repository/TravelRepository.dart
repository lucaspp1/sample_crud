import 'package:sample_crud/app/shared/SharedClasses.dart';
import 'package:sample_crud/app/shared/domain/Travel.dart';
import 'package:sample_crud/app/shared/repository/interface/ITravelRepository.dart';

class TravelRepository implements ITravelRepository {
  @override
  Future<bool> delete(Travel object) {
    return Future.value(SharedClasses.getListTravel().remove(object));
  }

  @override
  Future<bool> insert(Travel object) {
    SharedClasses.getListTravel().add(object);
    return Future.value(true);
  }

  @override
  Future<List<Travel>> searchAll() {
    return Future.value(SharedClasses.getListTravel());
  }

  @override
  Future<Travel> searchByid(int id) {
    return Future.value(SharedClasses.getListTravel().where((x) => x.id == id).first);
  }

  @override
  Future<bool> update(Travel object) {
    try {
      int index = SharedClasses.getListTravel().indexOf(
          SharedClasses.getListTravel().where((x) => x.id == object.id).first);
      SharedClasses.getListTravel()[index] = object;
      return Future.value(true);
    } on Exception catch (e) {
      return Future.value(false);
    }
  }
}
