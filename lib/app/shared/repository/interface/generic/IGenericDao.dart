abstract class IGenericDao<T>{
  Future<bool> insert(T object);
  Future<bool> update(T object);
  Future<bool> delete(T object);
  Future<T> searchByid(int id);
  Future<List<T>> searchAll();
}