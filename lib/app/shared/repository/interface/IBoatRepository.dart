import 'package:sample_crud/app/shared/domain/Boat.dart';
import 'package:sample_crud/app/shared/repository/interface/generic/IGenericDao.dart';

abstract class IBoatRepository implements IGenericDao<Boat> {}
