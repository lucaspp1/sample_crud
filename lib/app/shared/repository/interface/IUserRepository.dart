import 'package:sample_crud/app/shared/domain/User.dart';
import 'package:sample_crud/app/shared/repository/interface/generic/IGenericDao.dart';

abstract class IUserRepository implements IGenericDao<User> {}
