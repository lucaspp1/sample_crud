import 'package:sample_crud/app/shared/domain/Travel.dart';
import 'package:sample_crud/app/shared/repository/interface/generic/IGenericDao.dart';

abstract class ITravelRepository implements IGenericDao<Travel> {}
