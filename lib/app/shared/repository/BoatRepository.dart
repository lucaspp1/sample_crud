import 'package:sample_crud/app/shared/SharedClasses.dart';
import 'package:sample_crud/app/shared/domain/Boat.dart';
import 'package:sample_crud/app/shared/repository/interface/IBoatRepository.dart';
import 'dart:core';

class BoatRepository implements IBoatRepository {
  @override
  Future<bool> delete(Boat object) {
    return Future.value(SharedClasses.getListBoat().remove(object));
  }

  @override
  Future<bool> insert(Boat object) {
    SharedClasses.getListBoat().add(object);
    return Future.value(true);
  }

  @override
  Future<List<Boat>> searchAll() {
    return Future.value(SharedClasses.getListBoat());
  }

  @override
  Future<Boat> searchByid(int id) {
    return Future.value(SharedClasses.getListBoat().where((x) => x.id == id).first);
  }

  @override
  Future<bool> update(Boat object) {
    try {
      int index = SharedClasses.getListBoat().indexOf(
          SharedClasses.getListBoat().where((x) => x.id == object.id).first);
      SharedClasses.getListBoat()[index] = object;
      return Future.value(true);
    } on Exception catch (e) {
      return Future.value(false);
    }
  }
}
