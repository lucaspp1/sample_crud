import 'dart:core';

import 'domain/Boat.dart';
import 'domain/Travel.dart';
import 'domain/User.dart';

class SharedClasses{
  
  // Boats
  static List<Boat> _listBoat;
  static List<Boat> getListBoat(){
    if(_listBoat == null)
      _listBoat = List<Boat>();
    return _listBoat;
  }

  // Boats
  static List<User> _listUser;
  static List<User> getListUser(){
    if(_listUser == null)
      _listUser = List<User>();
    return _listUser;
  }

  // Users
  static List<Travel> _listTravel;
  static List<Travel> getListTravel(){
    if(_listTravel == null)
      _listTravel = List<Travel>();
    return _listTravel;
  }

}