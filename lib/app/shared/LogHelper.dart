import 'dart:developer';

class LogHelper<T> {
  void writeLog(String value){
    log(T.runtimeType.toString() + ": " + value);
  }
}