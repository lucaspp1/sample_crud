import 'package:sample_crud/app/shared/domain/Boat.dart';
import 'package:sample_crud/app/shared/domain/User.dart';

class Travel {
  int id;
  DateTime start, finish, expected;
  User user;
  Boat boat;

  bool _isRunnig;

  bool get isRunnig => finish == null;

  Travel(this.boat, this.user) {
    this.start = DateTime.now();
    this.id = new DateTime.now().millisecondsSinceEpoch;
  }

  Travel.full(this.boat, this.user, this.start, this.finish) {
    this.id = new DateTime.now().millisecondsSinceEpoch;
  }
  Travel.semiFull(this.boat, this.user, this.start) {
    this.id = new DateTime.now().millisecondsSinceEpoch;
  }
}
