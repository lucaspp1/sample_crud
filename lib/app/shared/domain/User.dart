class User {
  String name;
  int id;
  String email;

  User(this.name, this.email){
    this.id = new DateTime.now().millisecondsSinceEpoch;
  }
}