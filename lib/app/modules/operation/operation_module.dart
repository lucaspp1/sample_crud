import 'package:sample_crud/app/modules/operation/operation_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sample_crud/app/modules/operation/operation_page.dart';

class OperationModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => OperationController()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => OperationPage()),
      ];

  static Inject get to => Inject<OperationModule>.of();
}
