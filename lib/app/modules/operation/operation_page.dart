import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sample_crud/app/modules/operation/operation_controller.dart';

class OperationPage extends StatefulWidget {
  final String title;
  const OperationPage({Key key, this.title = "Operação"}) : super(key: key);

  @override
  _OperationPageState createState() => _OperationPageState();
}

class _OperationPageState extends State<OperationPage> {
  final OperationController operationController =
      Modular.get<OperationController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: 30),
              child: Text("Operation"),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: MaterialButton(
              onPressed: operationController.gotToBack,
              child: Text("Go to back"),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: Observer(builder: (_) {
              return operationController.isLoading
                  ? CircularProgressIndicator()
                  : Text("Not is loading");
            }),
          ),
        ],
      ),
    );
  }
}
