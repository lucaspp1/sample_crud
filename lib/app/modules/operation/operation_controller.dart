import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'operation_controller.g.dart';

class OperationController = _OperationBase with _$OperationController;

abstract class _OperationBase with Store {
  @observable
  bool isLoading = false;

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  @action
  void gotToBack() {
    isLoading = true;
    Future.delayed(Duration(seconds: 2))
        .then((x) => Modular.to.pushReplacementNamed("/"))
        .whenComplete(() => isLoading = false);
  }
}
