import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sample_crud/app/modules/home/home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeController homeController = HomeController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          _buildTitle(),
          _DefaultButton("Operacoes", "operation"),
          _DefaultButton("Administrações", "administrative"),
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: Observer(builder: (_) {
              return homeController.isLoading
                  ? CircularProgressIndicator()
                  : Text("Not is loading");
            }),
          ),
        ],
      ),
    );
  }

  // main title
  Widget _buildTitle() {
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 150),
        child: Column(
          children: <Widget>[
            Text(
              "CNC",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 100,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  // operation button
  Widget _DefaultButton(String name, String routeName) {
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: <Widget>[
            RaisedButton(
              color: Colors.blue,
              autofocus: true,
              onPressed: () => homeController.goToRoute(routeName),
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Text(
                  name,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
