import 'package:sample_crud/app/modules/administrative/administrative_module.dart';
import 'package:sample_crud/app/modules/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sample_crud/app/modules/home/home_page.dart';
import 'package:sample_crud/app/modules/operation/operation_module.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => HomePage()),
        Router("/administrative", module: AdministrativeModule(), transition: TransitionType.downToUp),
        Router("/operation", module: OperationModule(), transition: TransitionType.downToUp),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
