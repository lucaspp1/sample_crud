import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = _HomeBase with _$HomeController;

abstract class _HomeBase with Store {
  @observable
  int value = 0;

  @observable
  bool isLoading = false;

  @action
  void increment() {
    value++;
  }

  @action
  void goToRoute(String routeName) {
    isLoading = true;
    Future.delayed(Duration(seconds: 2))
        .then((_) => Modular.to.pushReplacementNamed(routeName))
        .whenComplete(() => isLoading = false);
  }
}
