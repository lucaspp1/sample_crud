import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sample_crud/app/modules/administrative/administrative_controller.dart';

class AdministrativePage extends StatefulWidget {
  final String title;
  const AdministrativePage({Key key, this.title = "Administração"})
      : super(key: key);

  @override
  _AdministrativePageState createState() => _AdministrativePageState();
}

class _AdministrativePageState extends State<AdministrativePage> {
  AdministrativeController administrativeController =
      Modular.get<AdministrativeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: Text("Operation"),
          ),
          MaterialButton(
            onPressed: administrativeController.gotToBack,
            child: Text("Go to back"),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: Observer(builder: (_) {
              return administrativeController.isLoading
                  ? CircularProgressIndicator()
                  : Text("Not is loading");
            }),
          ),
        ],
      ),
    );
  }
}
