import 'package:sample_crud/app/modules/administrative/administrative_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sample_crud/app/modules/administrative/administrative_page.dart';

class AdministrativeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AdministrativeController()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => AdministrativePage()),
      ];

  static Inject get to => Inject<AdministrativeModule>.of();
}
