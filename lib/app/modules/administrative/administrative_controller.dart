import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

part 'administrative_controller.g.dart';

class AdministrativeController = _AdministrativeBase
    with _$AdministrativeController;

abstract class _AdministrativeBase with Store {
  
  @observable
  bool isLoading = false;

  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }

  @action
  void gotToBack() {
    isLoading = true;
    Future.delayed(Duration(seconds: 2))
        .then((_) => Modular.to.pushReplacementNamed("/"))
        .whenComplete(() => isLoading = false);
  }
}
