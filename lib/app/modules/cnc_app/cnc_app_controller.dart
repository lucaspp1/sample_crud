import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:sample_crud/app/shared/LogHelper.dart';
import 'package:sample_crud/app/shared/domain/User.dart';
import 'package:sample_crud/app/shared/repository/UserRepository.dart';
import 'package:sample_crud/app/shared/repository/interface/IUserRepository.dart';

part 'cnc_app_controller.g.dart';

class CncAppController = _CncAppBase with _$CncAppController;

abstract class _CncAppBase with Store {
  final IUserRepository userRepository;
  final logHelper = LogHelper<CncAppController>();

  _CncAppBase(this.userRepository){

  }

  @observable
  bool isLoading = true;

  @observable
  List<User> listUser;

  @observable
  int value = 0;

  // FIELDS
  @observable
  String name;
  String email;

  @action
  void increment() {
    value++;
  }

  @action
  void initVariables() {
    logHelper.writeLog("initializing variables");
    userRepository.searchAll().then((value) => listUser = value).whenComplete(() => this.isLoading = false);
    logHelper.writeLog("initialized variables");
  }

  @action
  addUser() {
    logHelper.writeLog("adding user");
    userRepository.insert(User(name, email));
    logHelper.writeLog("user added");
  }

  void goBack(){
    logHelper.writeLog("going to back");
    Modular.to.pushReplacementNamed("/");
    logHelper.writeLog("back");
  }
}
