// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cnc_app_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CncAppController on _CncAppBase, Store {
  final _$isLoadingAtom = Atom(name: '_CncAppBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.context.enforceReadPolicy(_$isLoadingAtom);
    _$isLoadingAtom.reportObserved();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.context.conditionallyRunInAction(() {
      super.isLoading = value;
      _$isLoadingAtom.reportChanged();
    }, _$isLoadingAtom, name: '${_$isLoadingAtom.name}_set');
  }

  final _$listUserAtom = Atom(name: '_CncAppBase.listUser');

  @override
  List<User> get listUser {
    _$listUserAtom.context.enforceReadPolicy(_$listUserAtom);
    _$listUserAtom.reportObserved();
    return super.listUser;
  }

  @override
  set listUser(List<User> value) {
    _$listUserAtom.context.conditionallyRunInAction(() {
      super.listUser = value;
      _$listUserAtom.reportChanged();
    }, _$listUserAtom, name: '${_$listUserAtom.name}_set');
  }

  final _$valueAtom = Atom(name: '_CncAppBase.value');

  @override
  int get value {
    _$valueAtom.context.enforceReadPolicy(_$valueAtom);
    _$valueAtom.reportObserved();
    return super.value;
  }

  @override
  set value(int value) {
    _$valueAtom.context.conditionallyRunInAction(() {
      super.value = value;
      _$valueAtom.reportChanged();
    }, _$valueAtom, name: '${_$valueAtom.name}_set');
  }

  final _$nameAtom = Atom(name: '_CncAppBase.name');

  @override
  String get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$_CncAppBaseActionController = ActionController(name: '_CncAppBase');

  @override
  void increment() {
    final _$actionInfo = _$_CncAppBaseActionController.startAction();
    try {
      return super.increment();
    } finally {
      _$_CncAppBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void initVariables() {
    final _$actionInfo = _$_CncAppBaseActionController.startAction();
    try {
      return super.initVariables();
    } finally {
      _$_CncAppBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addUser() {
    final _$actionInfo = _$_CncAppBaseActionController.startAction();
    try {
      return super.addUser();
    } finally {
      _$_CncAppBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'isLoading: ${isLoading.toString()},listUser: ${listUser.toString()},value: ${value.toString()},name: ${name.toString()}';
    return '{$string}';
  }
}
