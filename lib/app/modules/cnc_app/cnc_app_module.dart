import 'package:sample_crud/app/modules/cnc_app/cnc_app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sample_crud/app/modules/cnc_app/cnc_app_page.dart';
import 'package:sample_crud/app/shared/repository/UserRepository.dart';
class CncAppModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => CncAppController(UserRepository())),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => CncAppPage()),
      ];

  static Inject get to => Inject<CncAppModule>.of();
}
