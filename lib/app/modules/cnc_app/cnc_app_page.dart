import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sample_crud/app/modules/cnc_app/cnc_app_controller.dart';
import 'package:sample_crud/app/shared/domain/User.dart';

class CncAppPage extends StatefulWidget {
  final String title;
  const CncAppPage({Key key, this.title = "CncApp"}) : super(key: key);

  @override
  _CncAppPageState createState() => _CncAppPageState();
}

class _CncAppPageState extends State<CncAppPage> {
  final controller = Modular.get<CncAppController>();

  @override
  void initState() {
    super.initState();
    controller.initVariables();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Observer(builder: (_) {
              return controller.isLoading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : Column(
                      children: <Widget>[
                        Center(
                          child: Text("Loading sucessfull"),
                        ),
                        Center(
                            child: MaterialButton(
                          onPressed: controller.goBack,
                          child: Text("Back"),
                          color: Color.fromARGB(250, 150, 200, 250),
                        ))
                      ],
                    );
            })));
  }
}
